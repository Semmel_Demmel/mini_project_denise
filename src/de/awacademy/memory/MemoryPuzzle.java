package de.awacademy.memory;

import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MemoryPuzzle extends Application {

    private static final int NUM_OF_PAIRS = 18;
    private static final int NUM_PER_ROW = 6;

    private Tile selected = null;
    private int clickCount = 2;

    public int score = 0;

    private Parent createContent() {
        Pane root = new Pane();
        root.setPrefSize(550, 600);


        char c = '\u263A';   // '\u880A
        List<Tile> tiles = new ArrayList<>();
        for (int i = 0; i < NUM_OF_PAIRS; i++) {
            tiles.add(new Tile(String.valueOf(c)));
            tiles.add(new Tile(String.valueOf(c)));
            c++;
        }

        Collections.shuffle(tiles);

        for (int i = 0; i < tiles.size(); i++) {
            Tile tile = tiles.get(i);
            tile.setTranslateX(85 * (i % NUM_PER_ROW));
            tile.setTranslateY(85 * (i / NUM_PER_ROW));
            root.getChildren().add(tile);
        }
        return root;
    }

    @Override
    public void start(Stage primaryStage) {

        System.out.println("Willkommen");

        try {

            Button b1 = new Button("Klick mich");

            HBox root = new HBox();
            root.getChildren().add(b1);

            b1.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    System.out.println("Hallo auch");
                }
            });

            StackPane root2 = new StackPane();
            Scene scene = new Scene(root2, 200, 200);

            primaryStage.setScene(new Scene(createContent()));
            primaryStage.sizeToScene();
            primaryStage.setTitle("Zeichen-Memory");
            primaryStage.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private class Tile extends StackPane {
        private Text text = new Text();



        public Tile(String value) {                        // hier kann Foto rein
            Rectangle border = new Rectangle(80, 80);
            border.setFill(Color.LAVENDER);
            border.setStroke(Color.BLUEVIOLET);


            text.setText(value);
            text.setFont(Font.font(45));
            text.setFill(Color.BLUEVIOLET);


            setAlignment(Pos.CENTER);
            getChildren().addAll(border, text);

            setOnMouseClicked(this::handleMouseClick);
            close();
        }

        public void handleMouseClick(MouseEvent event) {
            if (isOpen() || clickCount == 0)
                return;

            clickCount--;

            if (selected == null) {
                selected = this;
                open(() -> {});
            } else {
                open(() -> {
                    if (!hasSameValue(selected)) {
                        System.out.println("Pech");
                        selected.close();
                        this.close();
                    }
                    selected = null;
                    clickCount = 2;
                });
            }
        }

        public boolean isOpen() {
            return text.getOpacity() == 1;
        }

        public void open(Runnable action) {
            FadeTransition ft = new FadeTransition(Duration.seconds(0.3),text);
            ft.setToValue(1);
            ft.setOnFinished(e -> action.run());
            ft.play();
        }

        public void close() {
            FadeTransition ft = new FadeTransition(Duration.seconds(0.3),text);
            ft.setToValue(0);
            ft.play();
        }

        public boolean hasSameValue(Tile other) {

            if (text.getText().equals(other.text.getText())) {
                System.out.println("Yes, Baby!!!");
                score += 20;
                System.out.println("Score: " + score);
            } else {
                System.out.println("Ätsch!!!!");
                score -= 5;
                System.out.println("Score: " + score);
            }
            return text.getText().equals(other.text.getText());

        }
    }

    public static void main(String[] args) {

        launch(args);
    }

}
